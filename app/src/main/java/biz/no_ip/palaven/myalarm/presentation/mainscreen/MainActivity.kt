package biz.no_ip.palaven.myalarm.presentation.mainscreen

import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

import biz.no_ip.palaven.myalarm.R
import biz.no_ip.palaven.myalarm.app.App
import biz.no_ip.palaven.myalarm.presentation.mainscreen.adapter.AlarmsAdapter
import biz.no_ip.palaven.myalarm.repository.datamodel.Alarm
import butterknife.Bind
import butterknife.ButterKnife
import butterknife.OnClick
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), IMainView {

    @Bind(R.id.recycler_view_alarms)
    lateinit var rvAlarms: RecyclerView

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private var adapter: AlarmsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)
        initAlarmList()

        (application as App).graph?.inject(this)
    }

    private fun initAlarmList() {
        adapter = AlarmsAdapter()
        val layoutManager = LinearLayoutManager(this)
        rvAlarms.layoutManager = layoutManager
        rvAlarms.adapter = adapter
    }

    override fun showAlarms(alarms: List<Alarm>) {
        adapter!!.setDataSource(alarms)
    }

    @OnClick(R.id.button_add_alarm)
    fun addAlarm() {
        presenter.addMockAlarm()
    }
}
