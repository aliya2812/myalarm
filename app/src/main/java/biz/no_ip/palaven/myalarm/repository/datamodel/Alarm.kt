package biz.no_ip.palaven.myalarm.repository.datamodel

import io.realm.RealmObject
import java.util.*


open class Alarm: RealmObject() {

    var name: String = ""
    var startTime: Date = Date()
    var enabled: Boolean = false

    var id: Long = 0

}