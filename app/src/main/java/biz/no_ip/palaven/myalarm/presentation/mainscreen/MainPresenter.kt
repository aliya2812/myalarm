package biz.no_ip.palaven.myalarm.presentation.mainscreen

import biz.no_ip.palaven.myalarm.repository.datamodel.Alarm
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.realm.Realm
import java.util.*
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor() : MvpPresenter<IMainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        val realm = Realm.getDefaultInstance()
        val items = realm.where(Alarm::class.java).findAll()
        viewState.showAlarms(realm.copyFromRealm(items))
    }

    fun addMockAlarm() {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val alarm = realm.createObject(Alarm::class.java)
            alarm.id = 1
            alarm.name = "Example alarm 1"
            alarm.startTime = Date()
            alarm.enabled = true
        }
    }
}