package biz.no_ip.palaven.myalarm.presentation.mainscreen

import biz.no_ip.palaven.myalarm.repository.datamodel.Alarm
import com.arellomobile.mvp.MvpView

interface IMainView: MvpView {
    fun showAlarms(alarms: List<Alarm>)
}