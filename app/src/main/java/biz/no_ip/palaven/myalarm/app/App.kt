package biz.no_ip.palaven.myalarm.app

import android.app.Application
import biz.no_ip.palaven.myalarm.app.di.AppComponent
import biz.no_ip.palaven.myalarm.app.di.AppModule
import biz.no_ip.palaven.myalarm.app.di.DaggerAppComponent
import io.realm.Realm
import io.realm.RealmConfiguration

class App: Application() {

    var graph: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        initDagger()
        initRealm()

    }

    fun initDagger() {

        graph = DaggerAppComponent
                .builder()
                .appModule(AppModule())
                .build()

    }

    fun initRealm() {

        Realm.init(this)
        val config = RealmConfiguration.Builder().build()
        Realm.setDefaultConfiguration(config)

    }
}