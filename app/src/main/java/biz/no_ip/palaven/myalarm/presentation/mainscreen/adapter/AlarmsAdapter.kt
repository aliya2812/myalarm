package biz.no_ip.palaven.myalarm.presentation.mainscreen.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import biz.no_ip.palaven.myalarm.R
import biz.no_ip.palaven.myalarm.repository.datamodel.Alarm


class AlarmsAdapter: RecyclerView.Adapter<AlarmViewHolder>() {

    private var alarms: List<Alarm>? = null

    fun setDataSource(alarms: List<Alarm>) {
        this.alarms = alarms
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return alarms?.size?: 0
    }

    override fun onBindViewHolder(holder: AlarmViewHolder, position: Int) {
        val newsItem = alarms!![position]
        holder.bind(newsItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.alarm_item, parent, false)
        return AlarmViewHolder(view)
    }
}