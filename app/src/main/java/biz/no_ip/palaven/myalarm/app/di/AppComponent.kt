package biz.no_ip.palaven.myalarm.app.di

import biz.no_ip.palaven.myalarm.app.App
import biz.no_ip.palaven.myalarm.presentation.mainscreen.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(app: App)
    fun inject(mainActivity: MainActivity)

}