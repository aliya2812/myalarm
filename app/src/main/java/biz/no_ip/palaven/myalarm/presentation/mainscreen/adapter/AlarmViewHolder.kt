package biz.no_ip.palaven.myalarm.presentation.mainscreen.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import biz.no_ip.palaven.myalarm.R
import biz.no_ip.palaven.myalarm.repository.datamodel.Alarm
import java.text.SimpleDateFormat
import java.util.*


class AlarmViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val tvName: TextView? by lazy {
        itemView.findViewById(R.id.text_view_name) as TextView
    }

    private val tvTime: TextView? by lazy {
        itemView.findViewById(R.id.text_view_time) as TextView
    }

    private val cbEnabled: CheckBox? by lazy {
        itemView.findViewById(R.id.check_box_enabled) as CheckBox
    }

    var alarm: Alarm? = null

    fun bind(alarm: Alarm) {
        this.alarm = alarm
        tvName!!.text = alarm.name
        tvTime!!.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(alarm.startTime)
        cbEnabled!!.isChecked = alarm.enabled
    }
}